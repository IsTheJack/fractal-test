# Beer List
## Fractal

### Projeto teste para a vaga de Front End Developer da Fractal, por Roberto Oliveira
**url:** https://fractal-roberto.surge.sh

**PS1:** Como dito pelo email, não tive muita pressa de fazer para dar mais foco na qualidade do projeto como um todo.

Para rodar o projeto, basta realizar no terminal os seguintes comandos:
```
npm install && npm start
```

Node v8.9.4;

### CONFIGURAÇÃO DO PROJETO
#### React (Redux)
Foi utilizado React 16+ com Redux, dividido em módulos utilizando o seguinte pattern e bibliotecas desenvolvidos por mim:

* [Reducer Matching - A functional redux utility to replace the switch case statement.](https://github.com/IsTheJack/reducer-matching)
* [UFV Pattern — Indefinição, Inexistência e Presença de Valores em Componentes](https://medium.com/roliveiradev/react-ufv-pattern-indefini%C3%A7%C3%A3o-inexist%C3%AAncia-e-presen%C3%A7a-de-valores-em-componentes-c5e3b62cacec)

#### SASS (BEM)
Para manutenção do CSS, foi utilizado o SASS e o pattern BEM (Block Element Modifier), que combina perfeitamente com o sistemas de componentes do React, organizando o css de maneira simples e garantindo que não haverá interferência do css de um componente em outro.

#### Lint
Para padrão de código foi usado o EsLint do Airbnb

#### Testes
Para teste foi utilizado o **Jest** em conjunto com o **Enzyme** para testes simples de componente. Foram feitos testes de componente, testes das requisições do Axios, testes da função que abstrai a criação dinâmica de módulos Redux, testes das actions, das async actions e dos reduces.

#### Test Coverage
O projeto consta com **97% de cobertura de testes** para as funções criadas.

![Test Coverage](/doc/coverage.png)

**Para verificar a cobertura, basta executar o comando no terminal:**
```
npm run test -- --coverage
```

#### Deploy
O deploy é realizado no surge na url https://fractal-roberto.surge.sh/. Está redirecionando de http pra https normalmente.

#### Husky
Para assegurar que qualquer código que não passe no lint e nos testes suba para os remotes, foi usado o Husky para rodar Git hooks antes de cada commit e push. O push e commit não é realizado caso seja verificado código fora do padrão.

#### Sentry
Para fazer tracker de erros em tempo de execução no ambiente de produção, foi usado o Sentry.

![Sentry](/doc/sentry.png)

#### PWA (Progressive Web App)
O projeto consta com uma configuração para PWA instalando o Service Worker e garantindo HTTPS no deploy. Com isso é possível realizar Push Notifications, deixar disponível offline e permitir que o site seja instalável em devices que aceitem a tecnologia.

![Instalando o PWA em um Device Android](/doc/pwa.jpeg)

#### Hot Module Replacement
Através do Webpack, foi configurado o ‘Hot Swap’ de código para atualizar o código em desenvolvimento sem que sejam perdidos os dados do Redux.

#### Vendor Split
Foi configurado no webpack um arquivo para as bibliotecas e outro só para a aplicação, assim é possível deixar o arquivo de bibliotecas intacto, aproveitando o cache do navegador e aumentando a velocidade de carregamento.

### CircleCI
O Continuous Integration é feito usando o CircleCi, desse modo são rodados os testes automaticamente quando feito merge em certas branches no repositório remoto e é feito o deploy automaticamente quando mergeado na master.

![CircleCI](/doc/circleci.png)

#### Estrutura de Pastas
O projeto foi feito para se manter simples e pouco burocrático durante o processo de desenvolvimento, não criando muitas pastas para componentes, assim evitando a confusão e burocracia dos imports e dividindo o projeto em módulos de redux.

![Estrutura de Pastas](/doc/folders.png)

**Descrição do conteúdo da pasta src:**
* A pasta **\_\_mock\_\_** foi criada para a criação de mocks para testes.
* A pasta **assets** contém os arquivos estáticos como imagens, vídeos e fontes.
* A pasta **config** contém a configuração geral do Redux.
* A pasta **modules** é subdividida em pastas de módulos do projeto. Assim é possível dividir o projeto em módulos pequenos e que descrevem partes do sistema.
* A pasta **services** contém abstração para serviços externos como funções de chamadas à API’s e afins.

* O arquivo **favicon.png** é usado como favicon para web e ícone para a instalação do Web App em computadores e smartphones.
* O arquivo **index.html** é o template html usado pelo webpack para gerar o html final do projeto.
* O aquivo **index.js** é o arquivo de inicialização do projeto.
* O arquivo **index.scss** é o arquivo que gerencia todos os outros arquivos de sass do projeto que se encontram dentro da pasta styles em components.
* O arquivo **manifest.json** é usado para configurar dados importantes para o Web App.
* O arquivo **RouterConfig.js** é o componente de configuração de rotas do sistema.
* O arquivo **serviceWorkerRegistration.js** é um arquivo de instalação de Service Worker.

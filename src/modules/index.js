import app from './app';

const publicModules = [
  app,
];

export default publicModules;

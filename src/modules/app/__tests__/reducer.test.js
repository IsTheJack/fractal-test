import reducer, { initialState } from '../reducer';
import * as actions from '../actions';

describe('App reducer tests', () => {
  describe('on initApp reducer call', () => {
    it('must change the isAppReady to true', () => {
      const expectedResult = {
        ...initialState,
        isAppReady: true,
      };

      const action = actions.initApp();

      expect(reducer(undefined, action)).toEqual(expectedResult);
    });
  });

  describe('on storeBeers reducer call', () => {
    it('(UFV Pattern) must be undefined into initialState', () => {
      expect(initialState.beers).not.toBeDefined();
    });

    it('(UFV Pattern) must be false when beer list is empty', () => {
      const expectedResult = {
        ...initialState,
        beers: false,
      };

      const action = actions.fetchBeersSuccess([]);

      expect(reducer(undefined, action)).toEqual(expectedResult);
    });

    it('must store the beers in beers property', () => {
      const expectedResult = {
        ...initialState,
        beers: 'beers list',
      };

      const action = actions.fetchBeersSuccess('beers list');

      expect(reducer(undefined, action)).toEqual(expectedResult);
    });
  });

  describe('on noBeers reducer call', () => {
    it('(UFV Pattern) must be false when beer list is empty or on error', () => {
      const expectedResult = {
        ...initialState,
        beers: false,
      };

      const action = actions.fetchBeersFailure({});

      expect(reducer(undefined, action)).toEqual(expectedResult);
    });
  });
});

import * as actions from '../actions';

describe('App actions tests', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe('initApp', () => {
    it('must to return the correct value', () => {
      const expectedResult = {
        type: '[APP] INIT_APP',
      };

      expect(actions.initApp()).toEqual(expectedResult);
    });
  });

  describe('fetchBeersSuccess', () => {
    it('must to return the correct value', () => {
      const expectedResult = {
        type: '[APP] FETCH_BEERS_SUCCESS',
        payload: 'payload',
      };

      expect(actions.fetchBeersSuccess('payload')).toEqual(expectedResult);
    });
  });

  describe('fetchBeersFailure', () => {
    it('must to return the correct value', () => {
      const expectedResult = {
        type: '[APP] FETCH_BEERS_FAILURE',
        error: 'payload',
      };

      expect(actions.fetchBeersFailure('payload')).toEqual(expectedResult);
    });
  });

  describe('fetchBeers', () => {
    it('must to return the correct value', () => {
      const expectedResult = {
        type: '[APP] FETCH_BEERS',
      };

      expect(actions.fetchBeers()).toEqual(expectedResult);
    });
  });
});


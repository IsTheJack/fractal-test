import {
  runSaga,
} from 'redux-saga';

// Import Actions
import * as actions from '../actions';

import * as punkapi from '../../../services/punkapi';

import {
  fetchBeers,
} from '../sagas';

describe('app sagas', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe('fetchBeers saga function', () => {
    it('must to return correct success action on success operation', async () => {
      const dispatchMock = jest.fn();
      const successResultMock = { mock: 'mock' };

      jest
        .spyOn(punkapi, 'getBeers')
        .mockImplementation(() => Promise.resolve(successResultMock));

      await runSaga({
        dispatch: dispatchMock,
        getState: () => ({}),
      }, fetchBeers).done;

      const argOfFirstCall = dispatchMock.mock.calls[0][0];
      expect(argOfFirstCall).toEqual(actions.fetchBeersSuccess(successResultMock));
    });

    it('must to return correct failure action on failure operation', async () => {
      const dispatchMock = jest.fn();
      const successResultMock = { mock: 'mock' };

      jest
        .spyOn(punkapi, 'getBeers')
        .mockImplementation(() => Promise.reject(successResultMock));

      await runSaga({
        dispatch: dispatchMock,
        getState: () => ({}),
      }, fetchBeers).done;

      const argOfFirstCall = dispatchMock.mock.calls[0][0];
      expect(argOfFirstCall).toEqual(actions.fetchBeersFailure(successResultMock));
    });
  });
});

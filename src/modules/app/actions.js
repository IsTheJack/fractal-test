import * as types from './types';

export function initApp() {
  return {
    type: types.INIT_APP,
  };
}

export function fetchBeersSuccess(beers) {
  return {
    type: types.FETCH_BEERS_SUCCESS,
    payload: beers,
  };
}

export function fetchBeersFailure(error) {
  return {
    type: types.FETCH_BEERS_FAILURE,
    error,
  };
}

export function fetchBeers() {
  return {
    type: types.FETCH_BEERS,
  };
}

export const INIT_APP = '[APP] INIT_APP';

// Beers fetching
export const FETCH_BEERS = '[APP] FETCH_BEERS';
export const FETCH_BEERS_SUCCESS = '[APP] FETCH_BEERS_SUCCESS';
export const FETCH_BEERS_FAILURE = '[APP] FETCH_BEERS_FAILURE';

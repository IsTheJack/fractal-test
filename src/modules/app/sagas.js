import {
  all,
  put,
  call,
  takeLatest,
} from 'redux-saga/effects';

// Actions
import * as types from './types';
import * as actions from './actions';

// Services
import { getBeers } from '../../services/punkapi';

// Workers Sagas
export function* fetchBeers() {
  try {
    const beers = yield call(getBeers);
    yield put(actions.fetchBeersSuccess(beers));
  } catch (error) {
    yield put(actions.fetchBeersFailure(error));
  }
}

// Watchers Sagas
export function* watchFetchBeers() {
  yield takeLatest(types.FETCH_BEERS, fetchBeers);
}


// Root Saga
export default function* rootSaga() {
  yield all([
    watchFetchBeers(),
  ]);
}

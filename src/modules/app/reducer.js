import reducerMatching from 'reducer-matching';

import * as types from './types';

export const initialState = {
  isAppReady: false,
  beers: undefined,
};

export default function reducer(state = initialState, action) {
  return reducerMatching(
    [types.INIT_APP, isAppReady],
    [types.FETCH_BEERS, resetBeers],
    [types.FETCH_BEERS_SUCCESS, storeBeers],
    [types.FETCH_BEERS_FAILURE, noBeers],
  )(state, action);
}

// Reducers
function isAppReady(state) {
  return { ...state, isAppReady: true };
}

function resetBeers(state) {
  return { ...state, beers: initialState.beers };
}

function storeBeers(state, action) {
  const beers = action.payload.length ? action.payload : false;
  return { ...state, beers };
}

function noBeers(state) {
  return { ...state, beers: false };
}

import * as actions from './actions';
import * as constants from './constants';
import reducer from './reducer';
import * as types from './types';
import sagas from './sagas';

export default {
  actions,
  constants,
  reducer,
  types,
  sagas,
};

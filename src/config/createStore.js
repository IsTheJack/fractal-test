import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';

// Sentry
import Raven from 'raven-js';
import createRavenMiddleware from 'raven-for-redux';

import rootReducer from './rootReducer';
import publicModules from '../modules';

const devToolsComposeKey = '__REDUX_DEVTOOLS_EXTENSION_COMPOSE__';

const composeEnhancers = (
  process.env.NODE_ENV === 'development' &&
  window[devToolsComposeKey]
) || compose;

const sagaMiddleware = createSagaMiddleware();

const middleware = composeEnhancers(applyMiddleware(
  sagaMiddleware,
  createRavenMiddleware(Raven, {
    breadcrumbDataFromAction: action => action.type,
  }),
));

export default (data = {}) => {
  const store = createStore(rootReducer, data, middleware);

  // Applying Sagas from public modules
  publicModules.forEach(_module => _module.sagas && sagaMiddleware.run(_module.sagas));

  if (process.env.NODE_ENV !== 'production') {
    if (module.hot) {
      module.hot.accept('./rootReducer.js', () => {
        store.replaceReducer(rootReducer);
      });
    }
  }

  return store;
};

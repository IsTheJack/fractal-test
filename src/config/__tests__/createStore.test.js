import { dissoc } from 'ramda';
import createStore from '../createStore';
import publicModules from '../../modules/index';

describe('createStore function', () => {
  describe('initApp', () => {
    it('is not crashing', () => {
      createStore({});
    });

    it('Merges the initial state of public modules', () => {
      const store = createStore();
      const reducedState = publicModules
        .reduce((acc, _module) => ({ ...acc, [_module.constants.NAME]: _module.reducer(undefined, { type: 'mock' }) }), {});
      const storeWithPublicModules = dissoc('routing', store.getState());

      expect(storeWithPublicModules).toEqual(reducedState);
    });
  });
});


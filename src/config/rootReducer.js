import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import publicModules from '../modules';

const publicReducers = publicModules.reduce((reduceObj, _module) => ({
  ...reduceObj,
  [_module.constants.NAME]: _module.reducer,
}), {});

const rootReducer = combineReducers({
  ...publicReducers,
  routing: routerReducer,
});

export default rootReducer;

import React from 'react';
import PropTypes from 'prop-types';

// Assets
import beerImage from '../assets/img/beer.png';

const BeersListItem = ({ beer }) => (
  <li className="beers-list-item">
    <img
      className="beers-list-item__beer-icon"
      src={beerImage}
      alt="Beer"
    />

    <div>
      <div>Name: {beer.name}</div>
      <div>Tagline: {beer.tagline}</div>
    </div>
  </li>
);

BeersListItem.propTypes = {
  beer: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    tagline: PropTypes.string.isRequired,
  }).isRequired,
};

export default BeersListItem;

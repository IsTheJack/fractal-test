import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

// Actions
import * as appActions from '../modules/app/actions';

// Components
import BeersList from './BeersList';

class BeerScene extends Component {
  componentWillMount() {
    if (!this.props.appState.beers) { this.props.appActions.fetchBeers(); }
  }

  get emptyIndication() {
    return (
      <div className="beers-scene__indication">
        <p>
          Without Beers at moment!
        </p>

        <button
          className="beers-scene__refetch-beers-button"
          onClick={this.props.appActions.fetchBeers}
        >
          Find Beers
        </button>
      </div>
    );
  }

  get loadingIndication() {
    return (
      <div className="beers-scene__indication">
        <p>
          Fetching Beers...
        </p>
      </div>
    );
  }

  render() {
    const { beers } = this.props.appState;

    return (
      <div className="beers-scene">
        {beers === undefined && this.loadingIndication}
        {beers === false && this.emptyIndication}
        {beers && <BeersList beers={beers} />}
      </div>
    );
  }
}

BeerScene.propTypes = {
  appActions: PropTypes.objectOf(PropTypes.func).isRequired,
  appState: PropTypes.objectOf(PropTypes.any).isRequired,
};

const mapStateToProps = state => ({
  appState: state.app,
});

const mapDispatchToProps = dispatch => ({
  appActions: bindActionCreators(appActions, dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(BeerScene);

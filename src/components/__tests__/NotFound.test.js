import React from 'react';
import { shallow } from 'enzyme';

import NotFound from '../NotFound';

const wrapper = shallow(<NotFound />);

describe('(Component) NotFound', () => {
  it('renders without crash', () => {
    expect(wrapper).toHaveLength(1);
  });
});

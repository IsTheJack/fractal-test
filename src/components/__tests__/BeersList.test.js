import React from 'react';
import { shallow } from 'enzyme';

import BeersList from '../BeersList';

const wrapper = shallow(<BeersList beers={[{
  id: 1,
  name: 'Mock Name',
  tagline: 'Mock Tagline',
}]}
/>);

describe('(Component) BeersList', () => {
  it('renders without crash', () => {
    expect(wrapper).toHaveLength(1);
  });
});

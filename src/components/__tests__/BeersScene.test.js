import React from 'react';
import { shallow } from 'enzyme';
import BeersScene from '../BeersScene';

import createStore from '../../config/createStore';

describe('(Component) BeersScene', () => {
  it('renders without crash', () => {
    const wrapper = shallow(<BeersScene store={createStore()} />);
    expect(wrapper).toHaveLength(1);
  });

  it('must show the loading when start the scene', () => {
    const wrapper = shallow(<BeersScene store={createStore()} />);
    expect(wrapper.dive().find('.beers-scene__indication').text()).toBe('Fetching Beers...');
  });

  it('must show the refetch beers button when the fetch fails', () => {
    const wrapper = shallow(<BeersScene store={createStore({ app: { beers: false } })} />);
    expect(wrapper.dive().find('.beers-scene__refetch-beers-button')).toHaveLength(1);
  });
});

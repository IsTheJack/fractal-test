import React from 'react';
import { shallow } from 'enzyme';

import Dashboard from '../Dashboard';

const wrapper = shallow(<Dashboard><h1>Mock</h1></Dashboard>);

describe('(Component) Dashboard', () => {
  it('renders without crash', () => {
    expect(wrapper).toHaveLength(1);
  });
});

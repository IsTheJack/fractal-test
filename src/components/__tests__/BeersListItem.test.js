import React from 'react';
import { shallow } from 'enzyme';

import BeersListItem from '../BeersListItem';

const wrapper = shallow(<BeersListItem beer={{
  id: 1,
  name: 'Mock Name',
  tagline: 'Mock Tagline',
}}
/>);

describe('(Component) BeersListItem', () => {
  it('renders without crash', () => {
    expect(wrapper).toHaveLength(1);
  });
});

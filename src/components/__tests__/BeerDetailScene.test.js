import React from 'react';
import { shallow } from 'enzyme';
import BeerDetailScene from '../BeerDetailScene';

import createStore from '../../config/createStore';

describe('(Component) BeerDetailScene', () => {
  it('renders without crash', () => {
    const wrapper = shallow(<BeerDetailScene store={createStore()} />);
    expect(wrapper).toHaveLength(1);
  });

  it('must show the loading when start the scene', () => {
    const wrapper = shallow(<BeerDetailScene store={createStore()} />);
    expect(wrapper.dive().find('.beers-scene__indication').text()).toMatch('Fetching Beer...');
  });

  it('must show the refetch beers button when the fetch fails', () => {
    const wrapper = shallow(<BeerDetailScene store={createStore({ app: { beers: false } })} />);
    expect(wrapper.dive().find('.beers-scene__refetch-beers-button')).toHaveLength(1);
  });
});

import React from 'react';
import PropTypes from 'prop-types';

const Dashboard = ({ children }) => (
  <div className="dashboard">
    <div className="dashboard__children-container">
      {children}
    </div>
  </div>
);

Dashboard.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element,
  ]).isRequired,
};

export default Dashboard;

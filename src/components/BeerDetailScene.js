import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Link } from 'react-router';

// Actions
import * as appActions from '../modules/app/actions';

class BeerDetailScene extends Component {
  componentWillMount() {
    if (!this.props.appState.beers) { this.props.appActions.fetchBeers(); }
  }

  get beer() {
    const { beers } = this.props.appState;

    if (!beers) return beers;

    const { beerId } = this.props.params;

    return beers[beerId] || false;
  }

  get loadingIndication() {
    return (
      <div className="beers-scene__indication">
        <p>
          Fetching Beer...
        </p>

        {this.backLink}
      </div>
    );
  }

  get emptyIndication() {
    return (
      <div className="beers-scene__indication">
        <p>
          Beer is not found!
        </p>

        <button
          className="beers-scene__refetch-beers-button"
          onClick={this.props.appActions.fetchBeers}
        >
          Try Again
        </button>

        {this.backLink}
      </div>
    );
  }

  get backLink() {
    return (
      <Link to="/" href className="beer-detail-scene__go-back">Go back!</Link>
    );
  }

  render() {
    const { beer } = this;

    return (
      <div className="beer-detail-scene">
        {beer === undefined && this.loadingIndication}
        {beer === false && this.emptyIndication}
        {beer && (
          <div className="beer-detail-scene__content">
            <div className="beer-detail-scene__image-container">
              <img
                className="beer-detail-scene__image"
                src={beer.image_url}
                alt={beer.name}
              />
            </div>

            <div>
              <h1 className="beer-detail-scene__name">{beer.name}</h1>
              <h2 className="beer-detail-scene__tagline">{beer.tagline}</h2>
              <p className="beer-detail-scene__description">{beer.description}</p>
              {this.backLink}
            </div>
          </div>
        )}
      </div>
    );
  }
}

BeerDetailScene.propTypes = {
  appActions: PropTypes.objectOf(PropTypes.func).isRequired,
  appState: PropTypes.objectOf(PropTypes.any).isRequired,
  params: PropTypes.shape({
    beerId: PropTypes.string.isRequired,
  }).isRequired,
};

const mapStateToProps = state => ({
  appState: state.app,
});

const mapDispatchToProps = dispatch => ({
  appActions: bindActionCreators(appActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(BeerDetailScene);

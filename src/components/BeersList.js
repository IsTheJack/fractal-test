import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';

// Components
import BeersListItem from './BeersListItem';

const BeersList = ({ beers }) => (
  <ul className="beers-list">
    {beers.map(beer => (
      <Link
        to={`/${beer.id}`}
        href
        className="beers-list__anchor"
        key={`${beer.tagline}${beer.id}`}
      >
        <BeersListItem beer={beer} />
      </Link>
    ))}
  </ul>
);

BeersList.propTypes = {
  beers: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    tagline: PropTypes.string.isRequired,
  })).isRequired,
};

export default BeersList;

import React from 'react';
import ReactDOM from 'react-dom';

// Redux
import { Provider } from 'react-redux';

// Sentry
import './services/sentry';

// Service Worker
import './serviceWorkerRegistration';

// Component
import RouterConfig from './RouterConfig';
import './index.scss';

import createStore from './config/createStore';

const store = createStore({});

const rootEl = document.getElementById('root');

ReactDOM.render(
  <Provider store={store}>
    <RouterConfig store={store} key={Date.now()} />
  </Provider>,
  rootEl,
);

if (module.hot) {
  module.hot.accept('./RouterConfig', () => {
    const NewComponent = require('./RouterConfig').default; // eslint-disable-line
    ReactDOM.render(
      <Provider store={store}>
        <NewComponent store={store} key={Date.now()} />
      </Provider>,
      rootEl,
    );
  });
}


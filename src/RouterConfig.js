import React from 'react';
import { Router, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

// Components
import Dashboard from './components/Dashboard';
import BeersScene from './components/BeersScene';
import BeerDetailScene from './components/BeerDetailScene';
import NotFound from './components/NotFound';

export const routes = {
  childRoutes: [
    {
      component: Dashboard,
      childRoutes: [
        {
          path: '/',
          component: BeersScene,
        },
        {
          path: '/:beerId',
          component: BeerDetailScene,
        },
        {
          path: '*',
          component: NotFound,
        },
      ],
    },
  ],
};

const RouterConfig = ({
  store, // eslint-disable-line
}) => (
  <Router
    history={syncHistoryWithStore(browserHistory, store)}
    routes={routes}
  />
);

export default RouterConfig;

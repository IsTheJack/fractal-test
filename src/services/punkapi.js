import axios from 'axios';

const baseUrl = 'https://api.punkapi.com/v2';

// Endpoints
export const beerEndpoint = `${baseUrl}/beers`;

export function getBeers() {
  return axios.get(beerEndpoint)
    .then(res => res.data);
}

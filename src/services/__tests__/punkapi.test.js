import mockAxios from 'jest-mock-axios';

import * as punkapi from '../punkapi';

describe('punkapi service', () => {
  afterEach(() => {
    mockAxios.reset();
  });

  describe('getbeer function', () => {
    it('Must to return a promise', () => {
      const thenFn = jest.fn();
      const catchFn = jest.fn();

      const promise = punkapi.getBeers();

      promise
        .then(thenFn)
        .catch(catchFn);

      mockAxios.mockResponse({});

      expect(thenFn).toHaveBeenCalled();
      expect(catchFn).not.toHaveBeenCalled();
    });
  });
});


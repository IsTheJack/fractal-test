import Raven from 'raven-js';

Raven.config('https://fed228761de548b3aa088d76f33cbc3b@sentry.io/279588').install();

if (process.env.NODE_ENV !== 'production') Raven.uninstall();

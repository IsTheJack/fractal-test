const webpack = require('webpack');
const path = require('path');

// Plugins
const HtmlWebPackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const PUBLIC_PATH = 'https://fractal-roberto.surge.sh/'; // webpack needs the trailing slash for output.publicPath

const publicPathConfig = process.env.NODE_ENV === 'production' ? {
  publicPath: PUBLIC_PATH,
} : {};

module.exports = {
  entry: {
    vendor: [
      'axios',
      'prop-types',
      'ramda',
      'raven-for-redux',
      'raven-js',
      'react',
      'react-dom',
      'react-redux',
      'react-router',
      'react-router-redux',
      'reducer-matching',
      'redux',
      'redux-actions',
      'redux-thunk',
    ],
    app: './src/index.js',
  },

  output: Object.assign({
    path: path.resolve(__dirname, 'build'),
    publicPath: '/',
    filename: process.env.NODE_ENV === 'development' ?
      '[name].[hash].js' :
      '[name].[chunkhash].js',
  }, publicPathConfig),

  devServer: {
    historyApiFallback: true,
    contentBase: './build',
    hot: true,
  },

  devtool: 'source-map',

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
          },
        ],
      },
      {
        test: /\.scss$/,
        use: [{
          loader: 'style-loader',
        }, {
          loader: 'css-loader',
          options: {
            sourceMap: true,
          },
        }, {
          loader: 'sass-loader',
          options: {
            sourceMap: true,
          },
        }],
      },
      {
        test: /\.(png|jpg|gif|svg|mp4|webm)$/,
        exclude: /favicon\.png/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
            },
          },
        ],
      },
    ],
  },

  plugins: [
    new HtmlWebPackPlugin({
      template: './src/index.html',
      filename: './index.html',
    }),

    new webpack.optimize.CommonsChunkPlugin({ name: 'vendor' }),
    new webpack.optimize.CommonsChunkPlugin({ name: 'meta', chunks: ['vendor'] }),

    new ExtractTextPlugin({ // Extracting to a css file in production
      filename: '[name].[contenthash].css',
      disable: process.env.NODE_ENV === 'development',
    }),

    process.env.NODE_ENV === 'development' &&
      new webpack.NamedModulesPlugin(),

    process.env.NODE_ENV === 'development' &&
      new webpack.HotModuleReplacementPlugin(),

    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development'),
    }),

    process.env.NODE_ENV === 'production' &&
      new UglifyJsPlugin({ sourceMap: true }),

    process.env.NODE_ENV === 'production' && new SWPrecacheWebpackPlugin({
      cacheId: Date.now(),
      dontCacheBustUrlsMatching: /\.\w{8}\./,
      filename: 'service-worker.js',
      minify: true,
      navigateFallback: `${PUBLIC_PATH}index.html`,
      staticFileGlobsIgnorePatterns: [/\.map$/, /asset-manifest\.json$/],
    }),

  ].filter(Boolean),
};
